/* 
    Quick & dirty JS for the mal.ac overlords.
 */

const TimeFormatter = function(inSeconds) {
    let dayDuration = 60 * 60 * 24
    let hourDuration = 60 * 60

    let days = Math.floor(inSeconds / dayDuration)
    let hours = Math.floor((inSeconds % dayDuration) / hourDuration)
    let minutes = Math.floor((inSeconds % hourDuration) / 60)
    let seconds = inSeconds % 60

    let zeroPad = (n) => `${n < 10 ? '0' : ''}${n}`

    let dayDisplay = `${days ? days + 'd ' : ''}`
    let hourDisplay = `${dayDisplay}${hours ? zeroPad(hours) + ':' : dayDisplay ? '00:' : ''}`

    return `${hourDisplay}${zeroPad(minutes)}:${zeroPad(seconds)}`
}

const playAudioAtEnd = function() {
    let node = document.querySelector('#timerendinput')
    if (!node.files || !node.files.length) return;

    let file = node.files[0]
    let blob = window.URL.createObjectURL(file)

    let player = new window.Audio()
    player.src = blob
    player.play()
}

const setHeader = function() {
    let existingH1 = document.querySelector('h1')
    let h1 = (existingH1) ? existingH1 : document.createElement('h1')
    h1.innerText = decodeURIComponent(location.hash.slice(1))

    document.body.classList.add('txt')
    if (!existingH1) document.querySelector('#txt').prepend(h1)
}

const startTimer = function() {
    if (!window.location.search) return;

    let match = window.location.search.match(/timer=(\d+)/)
    if (!match || !match.length) return;

    let minutes = match[1]
    if (isNaN(minutes)) return;

    let h2 = document.createElement('h2')
    document.querySelector('#txt').appendChild(h2)

    let remainingSeconds = minutes * 60
    function tick() {
        document.querySelector('h2').innerText = TimeFormatter(remainingSeconds)
    }

    let interval = setInterval(function() {
        if (remainingSeconds <= 0) {
            playAudioAtEnd()
            return clearInterval(interval)
        }

        remainingSeconds--
        tick()
    }, 1000)

    tick()
}

const startCountdown = function() {
    if (!window.location.search) return;

    let match = window.location.search.match(/countdown=([^&]+)/)
    if (!match || !match.length) return;

    let dateStr = match[1]
    let d = new Date(dateStr)

    if (isNaN(d)) return;
    
    let h2 = document.createElement('h2')
    document.querySelector('#txt').appendChild(h2)
    
    let remainingSeconds = Math.floor((d.getTime() - Date.now()) / 1000)

    // Countdown date is in the past
    if (remainingSeconds < 0) {
        let d;
        if (!Intl || !Intl.RelativeTimeFormat) {
            d = (remainingSeconds * -1) + ' seconds ago'
        }
        else {
            let rtf = new Intl.RelativeTimeFormat(window.navigator.language)
            d = rtf.format(remainingSeconds, "second")
        }

        return document.querySelector('h2').innerText = `${d} 🐹`
    }

    function tick() {
        document.querySelector('h2').innerText = TimeFormatter(remainingSeconds)
    }

    let interval = setInterval(function() {
        if (remainingSeconds <= 0) {
            playAudioAtEnd()
            return clearInterval(interval)
        }

        remainingSeconds--
        tick()
    }, 1000)

    tick()
}

document.addEventListener('DOMContentLoaded', () => {
    if (window.location.hash) setHeader();
    window.addEventListener("hashchange", setHeader);

    if (window.location.search.indexOf('timer=') !== -1) {
        startTimer()
    }
    else if (window.location.search.indexOf('countdown=') !== -1) {
        startCountdown()
    }

    // Help "dialog" events
    let helpNode = document.querySelector('#help');

    document.addEventListener("keyup", (e) => {
        if (e.key == "?") helpNode.classList.toggle('show')
        if (e.key == "Escape") helpNode.classList.remove('show')
    })

    document.querySelector('#help').addEventListener("click", (e) => {
        if (e.target == helpNode) helpNode.classList.remove('show')
    })
})